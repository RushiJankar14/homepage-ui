import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { BlogComponent } from './blog/blog.component';
import { BrandComponent } from './brand/brand.component';
const routes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'Aboutus', component: AboutusComponent },
  { path: 'Blog', component: BlogComponent },
  { path: 'Brand', component: BrandComponent },

];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
